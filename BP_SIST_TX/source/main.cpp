#include "stm32f10x.h"
#include <stdbool.h>
#include "FatFs/ff.h"
#include "FatFs/diskio.h"
#include "FatFs/SPI_hardware.h"
#include "gpio.h"

#define LED_GPIO		GPIOC
#define LED_PIN			13

#define TX_SPIx 		SPI1
#define TX_GPIOx 		GPIOA
#define TX_CLK_PIN		5
#define TX_MISO_PIN		6
#define TX_MOSI_PIN		7
#define TX_ACK_PIN		4

const uint32_t BUFFER_SIZE = 4096;

void error(void)
{
	gpioBitSet(LED_GPIO, LED_PIN);
}

void noError(void)
{
	gpioBitReset(LED_GPIO, LED_PIN);
}

void spiInit(void)
{
	// SCK, MISO, MOSI
	gpioPinConfiguration(TX_GPIOx, TX_CLK_PIN,  GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED);
	gpioPinConfiguration(TX_GPIOx, TX_MOSI_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED);
	gpioPinConfiguration(TX_GPIOx, TX_MISO_PIN, GPIO_MODE_INPUT_FLOATING);
	gpioPinConfiguration(TX_GPIOx, TX_ACK_PIN, GPIO_MODE_INPUT_FLOATING);

	// init spi
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;

	TX_SPIx->CR1 = SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_CPHA | SPI_CR1_CPOL | SPI_CR1_BR_1;
	TX_SPIx->CR1 |= SPI_CR1_SPE;
}

bool spiIsAck(void)
{
	return gpioBitRead(TX_GPIOx, TX_ACK_PIN) == GPIO_TRUE;
}

uint8_t spiEx (uint8_t data)
{
	while(!(TX_SPIx->SR & SPI_SR_TXE));
	TX_SPIx->DR = data;
	while(!(TX_SPIx->SR & SPI_SR_RXNE));
	return TX_SPIx->DR;
}

void spiSendData( uint8_t *buffer, uint32_t bufferLen )
{
	for(uint32_t pos = 0; pos < bufferLen; pos++, buffer++)
		spiEx(*buffer);
}

void timeInit(void)
{
    NVIC_SetPriority(SysTick_IRQn, 0);
    SysTick->LOAD = SystemCoreClock/1000 - 1;
    SysTick->VAL = 0;
    SysTick->CTRL = SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk;
}

extern "C"
{
	__attribute__((interrupt)) void SysTick_Handler(void)
	{
		disk_timerproc();
	}
}

void main(void)
{
	timeInit();
	spiInit();
	gpioInitialize();
	gpioPinConfiguration(LED_GPIO, LED_PIN, GPIO_MODE_OUTPUT_PUSH_PULL_MEDIUM_SPEED);

	FATFS fatfs;
	FRESULT fr;
	FIL fil;

	uint8_t buffer[BUFFER_SIZE];

	bool errorWhileReading = false;

	for(;;)
	{
		fr = f_mount( &fatfs, "", 0 );

		if(fr != FR_OK)
		{
			error();
			continue;
		}

		fr = f_open(&fil, "PLIK.WAV", FA_READ);

		if(fr != FR_OK)
		{
			error();
			continue;
		}

		fr = f_lseek(&fil,80);

		if(fr != FR_OK)
		{
			error();
			continue;
		}

		while(1)
		{
			UINT br = 0;

			fr = f_read(&fil, buffer, BUFFER_SIZE, &br);

			if (fr != FR_OK)
			{
				errorWhileReading = true;
				break;
			}

			if(br != BUFFER_SIZE)
			{
				errorWhileReading = false;
				break;
			}

			while(!spiIsAck());
			//error();
			//for(;;);

			spiSendData( buffer, BUFFER_SIZE );
		}

		if(errorWhileReading)
		{
			errorWhileReading = false;
			error();
			continue;
		}
		else
		{

		}

		fr = f_close(&fil);

		if(fr != FR_OK)
		{
			error();
			continue;
		}

		fr = f_mount(0, "0:", 0);

		if(fr != FR_OK)
		{
			error();
			continue;
		}

		noError();
	}
}
