/*
 * SPI_hardware.h
 *
 *  Created on: Nov 5, 2016
 *      Author: BloedeBleidd
 */

#ifndef FATFS_SPI_HARDWARE_H_
#define FATFS_SPI_HARDWARE_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "stm32f10x.h"
#include "gpio.h"
#include "diskio.h"


// This function must be called from timer interrupt routine in period
// of 1 ms to generate card control timing.
void disk_timerproc (void);

/****************************** Configuration ******************************/

#define FATFS_SPIx 			SPI2
#define FATFS_GPIOx 		GPIOB
#define FATFS_CS_PIN		12
#define FATFS_CLK_PIN		13
#define FATFS_MISO_PIN		14
#define FATFS_MOSI_PIN		15

#define FATFS_SPI_PRESCALLER	(SPI_CR1_BR_1)

/*
 * 2   == (0x00)
 * 4   == (SPI_CR1_BR_0)
 * 8   == (SPI_CR1_BR_1)
 * 16  == (SPI_CR1_BR_0 | SPI_CR1_BR_1)
 * 32  == (SPI_CR1_BR_2)
 * 64  == (SPI_CR1_BR_0 | SPI_CR1_BR_2)
 * 128 == (SPI_CR1_BR_1 | SPI_CR1_BR_2)
 * 256 == (SPI_CR1_BR)
 */

/**************************** End Configuration ****************************/



#define FATFS_CS_HIGH 					FATFS_GPIOx->BSRR = FATFS_CS_PIN;
#define FATFS_CS_LOW 					FATFS_GPIOx->BRR = FATFS_CS_PIN;

#define FATFS_FCLK_SLOW() { FATFS_SPIx->CR1 = (FATFS_SPIx->CR1 & ~0x38) | SPI_CR1_BR; }	/* Set SCLK = AHB / 256 */
#define FATFS_FCLK_FAST() { FATFS_SPIx->CR1 = (FATFS_SPIx->CR1 & ~0x38) | FATFS_SPI_PRESCALLER; }

#define	FATFS_SPI_DATA					FATFS_SPIx->DR

#define	FATFS_SPI_TXE_FLAG				FATFS_SPIx->SR & SPI_SR_TXE
#define FATFS_SPI_WAIT_FOR_TXE_FLAG		while(!(FATFS_SPI_TXE_FLAG))

#define	FATFS_SPI_RXE_FLAG				FATFS_SPIx->SR & SPI_SR_RXNE
#define FATFS_SPI_WAIT_FOR_RXE_FLAG		while(!(FATFS_SPI_RXE_FLAG))

#define	FATFS_SPI_BUSY_FLAG				FATFS_SPIx->SR & SPI_SR_BSY
#define FATFS_SPI_WAIT_FOR_BUSY_FLAG	while((FATFS_SPI_BUSY_FLAG))

#define FATFS_SPI_16BIT_MODE			FATFS_SPIx->CR1 &= ~SPI_CR1_SPE; FATFS_SPIx->CR1 |= (SPI_CR1_DFF | SPI_CR1_SPE);
#define FATFS_SPI_8BIT_MODE				FATFS_SPIx->CR1 &= ~(SPI_CR1_DFF | SPI_CR1_SPE); FATFS_SPIx->CR1 |= SPI_CR1_SPE;

#ifdef __cplusplus
}
#endif

#endif /* FATFS_SPI_HARDWARE_H_ */
