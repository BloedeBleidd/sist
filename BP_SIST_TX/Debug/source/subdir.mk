################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../source/gpio.c 

CPP_SRCS += \
../source/main.cpp 

OBJS += \
./source/gpio.o \
./source/main.o 

C_DEPS += \
./source/gpio.d 

CPP_DEPS += \
./source/main.d 


# Each subdirectory must supply rules for building sources it contributes
source/%.o: ../source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32F1 -DSTM32F10X_MD -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\include" -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\CMSIS\core" -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\CMSIS\device" -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\libs" -std=gnu17 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

source/%.o: ../source/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32F1 -DSTM32F10X_MD -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\include" -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\CMSIS\core" -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\CMSIS\device" -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\libs" -std=gnu++17 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


