################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../libs/FatFs/SPI_hardware.c \
../libs/FatFs/ff.c 

OBJS += \
./libs/FatFs/SPI_hardware.o \
./libs/FatFs/ff.o 

C_DEPS += \
./libs/FatFs/SPI_hardware.d \
./libs/FatFs/ff.d 


# Each subdirectory must supply rules for building sources it contributes
libs/FatFs/%.o: ../libs/FatFs/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32F1 -DSTM32F10X_MD -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\include" -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\CMSIS\core" -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\CMSIS\device" -I"C:\AllPrograming\Eclipse\workspace\BP_SIST_TX\libs" -std=gnu17 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


