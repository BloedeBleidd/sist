################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../system/syscalls.c \
../system/system_stm32f10x.c 

S_UPPER_SRCS += \
../system/startup_stm32.S 

OBJS += \
./system/startup_stm32.o \
./system/syscalls.o \
./system/system_stm32f10x.o 

S_UPPER_DEPS += \
./system/startup_stm32.d 

C_DEPS += \
./system/syscalls.d \
./system/system_stm32f10x.d 


# Each subdirectory must supply rules for building sources it contributes
system/%.o: ../system/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -flto -fno-move-loop-invariants -Wall -Wextra  -g -x assembler-with-cpp -DSTM32F1 -DSTM32F10X_MD -I"C:\AllPrograming\Eclipse\workspace\BP\include" -I"C:\AllPrograming\Eclipse\workspace\BP\CMSIS\core" -I"C:\AllPrograming\Eclipse\workspace\BP\CMSIS\device" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

system/%.o: ../system/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -flto -fno-move-loop-invariants -Wall -Wextra  -g -DSTM32F1 -DSTM32F10X_MD -I"C:\AllPrograming\Eclipse\workspace\BP\include" -I"C:\AllPrograming\Eclipse\workspace\BP\CMSIS\core" -I"C:\AllPrograming\Eclipse\workspace\BP\CMSIS\device" -std=gnu17 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


