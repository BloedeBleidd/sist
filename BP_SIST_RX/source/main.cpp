#include "stm32f10x.h"
#include "gpio.h"

#define SAMPLE_RATE (22050)
#define RESOLUTION (10)
#define RESOLUTION_RANGE (1024)

#define LED_GPIO		GPIOC
#define LED_PIN			13

#define RX_SPIx 		SPI1
#define RX_GPIOx 		GPIOA
#define RX_CLK_PIN		5
#define RX_MISO_PIN		6
#define RX_MOSI_PIN		7
#define RX_ACK_PIN		4

#define SAMPLE_TIMER	TIM2

#define PWM_TIMER		TIM1
#define PWM_CH1 		TIM1->CCR1
#define PWM_CH2 		TIM1->CCR2
#define PWM_CH3 		TIM1->CCR3
#define PWM_CH4 		TIM1->CCR4

const uint32_t BUFFER_SIZE = 4096*2;

uint8_t buffer[BUFFER_SIZE];
volatile uint32_t offsetData;

void error(void)
{
	gpioBitSet(LED_GPIO, LED_PIN);
}

void noError(void)
{
	gpioBitReset(LED_GPIO, LED_PIN);
}

void spiSendAck(bool ack)
{
	GpioPinValue val = GPIO_FALSE;

	if( ack )
		val = GPIO_TRUE;

	gpioBitWrite(RX_GPIOx, RX_ACK_PIN, val);
}

void playerHardwareInit (void)
{
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

	gpioPinConfiguration(GPIOA,8,GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED);
	gpioPinConfiguration(GPIOA,9,GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED);
	gpioPinConfiguration(GPIOA,10,GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED);
	gpioPinConfiguration(GPIOA,11,GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED);

	PWM_TIMER->CCMR1 = TIM_CCMR1_OC1PE | TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC2PE | TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1;
	PWM_TIMER->CCMR2 = TIM_CCMR2_OC3PE | TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC4PE | TIM_CCMR2_OC4M_2 | TIM_CCMR2_OC4M_1;
	PWM_TIMER->CCER = TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E;
	PWM_TIMER->BDTR = TIM_BDTR_MOE;
	PWM_TIMER->ARR = RESOLUTION_RANGE-1;
	PWM_TIMER->EGR = TIM_EGR_UG;
	PWM_TIMER->CR1 = TIM_CR1_ARPE;
	//on
	PWM_TIMER->CR1 |= TIM_CR1_CEN;

	SAMPLE_TIMER->DIER = TIM_DIER_UIE;
	SAMPLE_TIMER->ARR = (uint16_t)(((uint32_t)SystemCoreClock / SAMPLE_RATE)-1);
	//on
	SAMPLE_TIMER->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM2_IRQn);
}

void spiInit(void)
{
	// SCK, MISO, MOSI
	gpioPinConfiguration(RX_GPIOx, RX_CLK_PIN,  GPIO_MODE_ALTERNATE_OPEN_DRAIN_HIGH_SPEED);
	gpioPinConfiguration(RX_GPIOx, RX_MOSI_PIN, GPIO_MODE_ALTERNATE_OPEN_DRAIN_HIGH_SPEED);
	gpioPinConfiguration(RX_GPIOx, RX_MISO_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED);
	gpioPinConfiguration(RX_GPIOx, RX_ACK_PIN, GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED);

	// init spi
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;

	RX_SPIx->CR1 = SPI_CR1_SSM | SPI_CR1_CPHA | SPI_CR1_CPOL | SPI_CR1_BR_1;
	RX_SPIx->CR1 |= SPI_CR1_SPE;
}

uint8_t spiEx (uint8_t data)
{
	while(!(RX_SPIx->SR & SPI_SR_TXE));
	RX_SPIx->DR = data;
	while(!(RX_SPIx->SR & SPI_SR_RXNE));
	return RX_SPIx->DR;
}

void spiReadData( uint8_t *buffer, uint32_t bufferLen )
{
	for(uint32_t pos = 0; pos < bufferLen; pos++, buffer++)
		*buffer = spiEx(0);
}

extern "C"
{
	__attribute__((interrupt)) void TIM2_IRQHandler (void)
	{
		if(SAMPLE_TIMER->SR & TIM_SR_UIF)
		{
			SAMPLE_TIMER->SR &= ~TIM_SR_UIF;

			uint16_t buf = buffer[offsetData] | (buffer[offsetData+1] << 8);
			int16_t bufSigned = (int16_t)buf;

			uint16_t sample = (bufSigned+32768) >> (16-RESOLUTION);

			PWM_CH4 = sample;

			offsetData += 2;

			if (offsetData >= BUFFER_SIZE)
				offsetData = 0;
		}
	}
}

void main(void)
{
	gpioInitialize();
	gpioPinConfiguration(LED_GPIO, LED_PIN, GPIO_MODE_OUTPUT_PUSH_PULL_MEDIUM_SPEED);
	spiSendAck(false);
	spiInit();
	playerHardwareInit();

	bool switcherFlag = false;

	for(;;)
	{
		if(!switcherFlag && offsetData>((BUFFER_SIZE/2)-1))
		{
			switcherFlag = true;
			spiSendAck(true);
			spiReadData(buffer, BUFFER_SIZE/2);
			spiSendAck(false);
		}

		if(switcherFlag && offsetData<(BUFFER_SIZE/2))
		{
			switcherFlag = false;
			spiSendAck(true);
			spiReadData(&buffer[BUFFER_SIZE/2], BUFFER_SIZE/2);
			spiSendAck(false);
		}
	}
}
